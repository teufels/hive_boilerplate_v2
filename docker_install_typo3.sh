#!/bin/bash

echo ""
echo "HIVE | installer\n"

echo "composer install"

#wget https://getcomposer.org/composer.phar
chmod 777 ./composer.phar && ./composer.phar install

echo "cd /app/web/typo3conf/ext/\n"

cd /app/web/typo3conf/ext/

echo "ln -s ../../../vendor/helhum/typo3-console typo3_console\n"

ln -s ../../../vendor/helhum/typo3-console typo3_console

echo "cd /app/\n"

cd /app/

echo "install typo3"

./web/typo3conf/ext/typo3_console/Scripts/typo3cms install:setup \
    --non-interactive \
    --use-existing-database \
    --database-user-name="dev" \
    --database-host-name="mysql" \
    --database-port="3306" \
    --database-name="dev" \
    --admin-user-name="sudo" \
    --admin-password="Jai_Fee4Eewohci?um7a" \
    --site-name="Basic Install"

echo ""
echo "Activate all extensions"
echo ""

./web/typo3conf/ext/typo3_console/Scripts/typo3cms install:generatepackagestates \
    --activate-default

echo ""
echo "User: sudo"
echo "Password: Jai_Fee4Eewohci?um7a (please change this by importing dummy db)"

echo ""
echo "Done."