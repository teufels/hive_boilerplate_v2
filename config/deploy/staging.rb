server '<teufels.firma.cc>', user: 'ssh-<user>', roles: %w{web}, ssh_options: { forward_agent: true }

##
## :teufels_staging_date
##
## e.g. set :teufels_staging_date, "<yyyymmddhhmm>"
##
set :teufels_staging_date, "<yyyymmddhhmm>"

##
## :teufels_db_ini
##
## e.g. set :teufels_db_ini, "staging_1.ini"
##
set :teufels_db_ini, "<staging_1.ini>"

##
## :teufels_customer_abbr
##
## Customer Abbr.
## e.g. set :deploy_to, "teu"
##
set :teufels_customer_abbr, "<customer_abbr>"

##
## :teufels_type
##
## e.g. set :teufels_type, "ws"
##
set :teufels_type, "ws"

##
## :teufels_year
##
## e.g. set :teufels_year, "17"
##
set :teufels_year, "17"

##
## :teufels_stage
##
## e.g. set :teufels_stage, "staging"
##
set :teufels_stage, "staging"

##
## :teufels_node_modules_bin
##
## Absolute path to node_modules
## e.g. set :teufels_node_modules_bin, "./node_modules/.bin"
##
set :teufels_node_modules_bin, "./node_modules/.bin"

##
## :teufels_php_cli_bin
##
## Absolute path to PHP CLI
## e.g. set :teufels_php_cli_bin, "/usr/local/bin/php7-70STABLE-CLI"
##
set :teufels_php_cli_bin, "/usr/local/bin/php7-70STABLE-CLI"

##
## :teufels_php_bin
##
## Absolute path to PHP
## e.g. set :teufels_php_bin, "/usr/local/bin/php7-70STABLE-STANDARD"
## e.g. set :teufels_php_bin, "/usr/local/bin/php7-70STABLE-FCGI"
##
set :teufels_php_bin, "/usr/local/bin/php7-70STABLE-STANDARD"

##
## :teufels_server_path
##
## Absolute server path
## e.g. set :deploy_to, "/www/470890_78628/webseiten"
##
set :teufels_server_path, "/www/470890_78628/webseiten"

##
## :teufels_quota
##
## Absolute path to quota
## e.g. set :teufels_quota, "#{fetch(:teufels_server_path)}/#{fetch(:teufels_customer_abbr)}#{fetch(:teufels_type)}"
##
set :teufels_quota, "#{fetch(:teufels_server_path)}/#{fetch(:teufels_customer_abbr)}/#{fetch(:teufels_type)}"

##
## :teufels_fileadmin
## :teufels_uploads
## 
## Absolute paths to shared folders (e.g. fileadmin, uploads,...) 
##
## set :teufels_fileadmin, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/fileadmin/#{fetch(:teufels_staging_date)}"
## set :teufels_uploads, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/uploads/#{fetch(:teufels_staging_date)}"
##
## IMPORTANT !!!
## Add shared folders in production.rb and ../deploy.rb
##
set :teufels_fileadmin, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/fileadmin/#{fetch(:teufels_staging_date)}" 
set :teufels_uploads, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/uploads/#{fetch(:teufels_staging_date)}"

################################################################################################################################
################################################################################################################################
##
## Do not edit anything below or clowns will eat you
##
################################################################################################################################
################################################################################################################################

##
## :deploy_to
##
## Absolute path to quota + year and staging folder
## e.g. set :deploy_to, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/staging"
##
set :deploy_to, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}"

##
## :teufels_config
## :teufels_database_ini
## :teufels_sudo_ini
## :teufels_database_dump_definition
## :teufels_database_dump_data
##
## Absolute path to shared config / ini / dump
## e.g. set :teufels_config, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/config"
## e.g. set :teufels_database_ini, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/config/#{fetch(:teufels_db_ini)}"
## e.g. set :teufels_sudo_ini, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/config/sudo.ini"
## e.g.set :teufels_database_dump_definition, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/dump/#{fetch(:teufels_staging_date)}/definition.sql"
## e.g.set :teufels_database_dump_data, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/dump/#{fetch(:teufels_staging_date)}/data.sql"
##
set :teufels_config, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/config" 
set :teufels_database_ini, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/config/#{fetch(:teufels_db_ini)}"
set :teufels_sudo_ini, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/config/sudo.ini"
set :teufels_database_dump_definition, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/dump/#{fetch(:teufels_staging_date)}/definition.sql"
set :teufels_database_dump_data, "#{fetch(:teufels_quota)}/#{fetch(:teufels_year)}/#{fetch(:teufels_stage)}/shared/dump/#{fetch(:teufels_staging_date)}/data.sql"

namespace :staging do

  task :teufels_staging do
    on roles(:web) do
      # nothing right now
    end
  end

  after 'deploy:finishing', 'staging:teufels_staging'
end