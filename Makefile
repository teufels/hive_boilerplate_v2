ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Docker machine states
#############################

up:
	docker-compose up -d

start:
	docker-compose start

stop:
	docker-compose stop

state:
	docker-compose ps

rebuild:
	docker-compose stop
	docker-compose rm --force app
	docker-compose build --no-cache
	docker-compose up -d

#############################
# Docker machine states
#############################

up-on-linux:
	docker-compose -f docker-compose.linux.yml up -d

start-on-linux:
	docker-compose -f docker-compose.linux.yml start

stop-on-linux:
	docker-compose -f docker-compose.linux.yml stop

state-on-linux:
	docker-compose -f docker-compose.linux.yml ps

rebuild-on-linux:
	docker-compose -f docker-compose.linux.yml stop
	docker-compose -f docker-compose.linux.yml rm --force app
	docker-compose -f docker-compose.linux.yml build --no-cache
	docker-compose -f docker-compose.linux.yml up -d

#############################
# Docker machine states
#############################

up-on-osx:
	docker-compose -f docker-compose.osx.yml up -d

start-on-osx:
	docker-compose -f docker-compose.osx.yml start

stop-on-osx:
	docker-compose -f docker-compose.osx.yml stop

state-on-osx:
	docker-compose -f docker-compose.osx.yml ps

rebuild-on-osx:
	docker-compose -f docker-compose.osx.yml stop
	docker-compose -f docker-compose.osx.yml rm --force app
	docker-compose -f docker-compose.osx.yml build --no-cache
	docker-compose -f docker-compose.osx.yml up -d

#############################
# General
#############################

shell:
	docker exec -it -u www-data $$(docker-compose ps -q app) /bin/zsh

root:
	docker exec -it -u root $$(docker-compose ps -q app) /bin/zsh

apache:
	docker exec -it $$(docker-compose ps -q apache2) bash

gulp:
	docker exec -it -u root $$(docker-compose ps -q phpgulp) /bin/zsh

#############################
# General
#############################

shell-on-linux:
	docker exec -it -u www-data $$(docker-compose -f docker-compose.linux.yml ps -q app) /bin/zsh

root-on-linux:
	docker exec -it -u root $$(docker-compose -f docker-compose.linux.yml ps -q app) /bin/zsh

apache-on-linux:
	docker exec -it $$(docker-compose -f docker-compose.linux.yml ps -q apache2) bash

gulp-on-linux:
	docker exec -it -u root $$(docker-compose -f docker-compose.linux.yml ps -q phpgulp) /bin/zsh

#############################
# General
#############################

shell-on-osx:
	docker exec -it -u www-data $$(docker-compose -f docker-compose.osx.yml ps -q app) /bin/zsh

root-on-osx:
	docker exec -it -u root $$(docker-compose -f docker-compose.osx.yml ps -q app) /bin/zsh

apache-on-osx:
	docker exec -it $$(docker-compose -f docker-compose.osx.yml ps -q apache2) bash

gulp-on-osx:
	docker exec -it -u root $$(docker-compose -f docker-compose.osx.yml ps -q phpgulp) /bin/zsh

#############################
# Argument fix workaround
#############################
%:
	@:
