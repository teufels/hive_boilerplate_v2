Please copy staging_x.ini. Then change content.

```
cp staging_x.ini staging_1.ini
cp staging_x.ini staging_2.ini
cp staging_x.ini staging_3.ini
cp staging_x.ini staging_4.ini
cp staging_x.ini staging_5.ini
```

Please copy sudo_dummy.ini. Then change content.  Do _not_ use "$" in password.

```
cp sudo_dummy.ini sudo.ini
```
