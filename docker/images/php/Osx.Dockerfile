#+++++++++++++++++++++++++++++++++++++++
# Dockerfile for teufels/hny_php_fpm:7.1
#+++++++++++++++++++++++++++++++++++++++

FROM teufels/docker_php_fpm:7.1

# link xdebug config for osx
RUN ln -sf /opt/docker/etc/php/xdebugfordockerformac.ini /usr/local/etc/php/conf.d/xdebugfordockerformac.ini